from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, BooleanField, SelectField,\
    SubmitField, IntegerField
from wtforms.validators import Required, Length, Email, Regexp
from wtforms import ValidationError
from flask_pagedown.fields import PageDownField
from ..models import Role, User


class NameForm(FlaskForm):
    name = StringField('What is your name?', validators=[Required()])
    submit = SubmitField('Submit')


class EditProfileForm(FlaskForm):
    name = StringField('Real name', validators=[Length(0, 64)])
    location = StringField('Location', validators=[Length(0, 64)])
    about_me = TextAreaField('About me')
    submit = SubmitField('Submit')


class EditProfileAdminForm(FlaskForm):
    email = StringField('Email', validators=[Required(), Length(1, 64),
                                             Email()])
    username = StringField('Username', validators=[
        Required(), Length(1, 64), Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
                                          'Usernames must have only letters, '
                                          'numbers, dots or underscores')])
    confirmed = BooleanField('Confirmed')
    role = SelectField('Role', coerce=int)
    name = StringField('Real name', validators=[Length(0, 64)])
    location = StringField('Location', validators=[Length(0, 64)])
    about_me = TextAreaField('About me')
    submit = SubmitField('Submit')

    def __init__(self, user, *args, **kwargs):
        super(EditProfileAdminForm, self).__init__(*args, **kwargs)
        self.role.choices = [(role.id, role.name)
                             for role in Role.query.order_by(Role.name).all()]
        self.user = user

    def validate_email(self, field):
        if field.data != self.user.email and \
                User.query.filter_by(email=field.data).first():
            raise ValidationError('Email already registered.')

    def validate_username(self, field):
        if field.data != self.user.username and \
                User.query.filter_by(username=field.data).first():
            raise ValidationError('Username already in use.')


class PostForm(FlaskForm):
    body = PageDownField("What's on your mind?", validators=[Required()])
    submit = SubmitField('Submit')


class CommentForm(FlaskForm):
    body = StringField('Enter your comment', validators=[Required()])
    submit = SubmitField('Submit')

class PayForm(FlaskForm):
    # email = StringField('Email', validators=[Required(), Length(1, 64),
    #                                          Email()])
    # username = StringField('Username', validators=[
    #     Required(), Length(1, 64), Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
    #                                       'Usernames must have only letters, '
    #                                       'numbers, dots or underscores')])
    # confirmed = BooleanField('Confirmed')
    # role = SelectField('Role', coerce=int)
    name = StringField('Full Name', validators=[Required(), Length(1, 64)])
    country = StringField('Country', validators=[Required(), Length(1, 84)])
    address = StringField('Address', validators=[Required(), Length(1, 128)])
    city = StringField('City', validators=[Required(), Length(1, 64)])
    phone = StringField('Phone', validators=[Required(), Length(4, 30)])
    # zip = StringField('Zip Code', validators=[Required()])
    postal_zip = StringField('Postal Code/Zip Code', validators=[Required()])
    # number = IntegerField('Card Number', validators=[Required(), Length(15, 19)])
    submit = SubmitField('Next')

    # expiration_dates = self.generate_expiration_dates()

    def generate_expiration_dates(self): ## for drop down menu
        import datetime
        current_date = datetime.datetime.now()
        expiration_dates = [date for date in range(current_date.year, current_date.year + 21, 1)]
        return expiration_dates
    #
    # def __init__(self, user, *args, **kwargs):
    #      super(PayForm, self).__init__(*args, **kwargs)
    #      # self.role.choices = [(role.id, role.name)
    #      #                      for role in Role.query.order_by(Role.name).all()]
    #      self.user = user
    #
    # def validate_email(self, field):
    #     if field.data != self.user.email and \
    #             User.query.filter_by(email=field.data).first():
    #         raise ValidationError('Email already registered.')
    #
    # def validate_username(self, field):
    #     if field.data != self.user.username and \
    #             User.query.filter_by(username=field.data).first():
    #         raise ValidationError('Username already in use.')
